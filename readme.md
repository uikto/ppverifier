Dieses Projekt verwendet Maven zur Abhängigkeitsverwaltung und als Buildsystem.
Das Programm kann mit `mvn compile exec:java` kompiliert und ausgeführt werden.
Automatische Tests können mit `mvn test` ausgeführt werden.
Diese Tests enthalten alle in der Bachelorarbeit beschriebenen Beispiele.
Es sei darauf hingewiesen, dass die Ausführung der SystemTests je nach Hardware eine halbe Stunde in Anspruch nimmt.
Um die besonders langen Tests zu überspringen nutze `NO_LONG_TESTS=1;mvn test` unter Linux.
Eine CI-Pipeline, die genau das tut ist in `.gitlab-ci.yml` definiert.

Beispiele zum testen:
Zs8 nicht ohne Zs 6
```
∀ Signal_Rahmen:
    (∃ Signal_Signalbegriff: 
        Signal_Signalbegriff.Signal_Signalbegriff_Allg.Geschaltet 
        ∧ Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_6"
        ∧ Signal_Rahmen == Signal_Signalbegriff.ID_Signal_Rahmen)
    ∨ (∄ Signal_Signalbegriff:
        Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_8"
        ∧ Signal_Rahmen == Signal_Signalbegriff.ID_Signal_Rahmen)
```
Dies sollte zum folgenden Resultat führen:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    <ns uri="http://www.plan-pro.org/modell/PlanPro/1.10.0.1" prefix="nsPlanPro"/>
    <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <ns uri="http://www.w3.org/2005/xpath-functions" prefix="fn"/>
    <pattern>
        <rule context="LST_Planung">
            <assert test="(every $Signal_Rahmen in //LST_Zustand_Ziel/Container/descendant::Signal_Rahmen satisfies ((some $Signal_Signalbegriff in //LST_Zustand_Ziel/Container/descendant::Signal_Signalbegriff satisfies ((($Signal_Signalbegriff/Signal_Signalbegriff_Allg/Geschaltet/Wert='true') and (substring($Signal_Signalbegriff/Signalbegriff_ID/@xsi:type, 26) = 'Zs_6')) and ($Signal_Rahmen/Identitaet/Wert = //LST_Zustand_Ziel/Container/descendant::*[./Identitaet/Wert=($Signal_Signalbegriff/ID_Signal_Rahmen/Wert)]/Identitaet/Wert))) or (not((some $Signal_Signalbegriff in //LST_Zustand_Ziel/Container/descendant::Signal_Signalbegriff satisfies ((substring($Signal_Signalbegriff/Signalbegriff_ID/@xsi:type, 26) = 'Zs_8') and ($Signal_Rahmen/Identitaet/Wert = //LST_Zustand_Ziel/Container/descendant::*[./Identitaet/Wert=($Signal_Signalbegriff/ID_Signal_Rahmen/Wert)]/Identitaet/Wert)))))))"/>
        </rule>
    </pattern>
</schema>
```

Wenn man den generierten XPath Ausdruck passend einrückt kann man die korrektheit besser prüfen,
```xpath2
(every $Signal_Rahmen in //LST_Zustand_Ziel/Container/descendant::Signal_Rahmen satisfies (
    (some $Signal_Signalbegriff in //LST_Zustand_Ziel/Container/descendant::Signal_Signalbegriff satisfies (
         (($Signal_Signalbegriff/Signal_Signalbegriff_Allg/Geschaltet/Wert='true') and
         (substring($Signal_Signalbegriff/Signalbegriff_ID/@xsi:type, 26) = 'Zs_6')) and
         ($Signal_Rahmen/Identitaet/Wert = //LST_Zustand_Ziel/Container/descendant::*[./Identitaet/Wert=($Signal_Signalbegriff/ID_Signal_Rahmen/Wert)]/Identitaet/Wert)))  
     or (not(
     (some $Signal_Signalbegriff in //LST_Zustand_Ziel/Container/descendant::Signal_Signalbegriff satisfies (
        (substring($Signal_Signalbegriff/Signalbegriff_ID/@xsi:type, 26) = 'Zs_8') and 
        ($Signal_Rahmen/Identitaet/Wert = //LST_Zustand_Ziel/Container/descendant::*[./Identitaet/Wert=($Signal_Signalbegriff/ID_Signal_Rahmen/Wert)]/Identitaet/Wert)))))))
```
Oder das Zs 3 / Zs 3v Beispiel:
```
zs3_nicht_kombiniert::=
∀ Signal: (
    ∄ Signal_Rahmen:
        Signal==Signal_Rahmen.ID_Signal ∧ 
        ∃ Signal_Signalbegriff: 
            Signal_Signalbegriff.ID_Signal_Rahmen==Signal_Rahmen ∧ 
            Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_3" ∧
            ¬Signal_Signalbegriff.Signal_Signalbegriff_Allg.Geschaltet) 
    ∨ (∄ Signal_Rahmen:
        Signal==Signal_Rahmen.ID_Signal ∧ 
        ∃ Signal_Signalbegriff: 
            Signal_Signalbegriff.ID_Signal_Rahmen==Signal_Rahmen ∧ 
            Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_3v" ∧ 
            Signal_Signalbegriff.Signal_Signalbegriff_Allg.Geschaltet)
```

Dies sollte zu folgendem Ergebnis führen
```xml
<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    <ns uri="http://www.plan-pro.org/modell/PlanPro/1.10.0.1" prefix="nsPlanPro"/>
    <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <ns uri="http://www.w3.org/2005/xpath-functions" prefix="fn"/>
    <pattern>
        <rule context="LST_Planung">
            <assert test="(every $Signal in //LST_Zustand_Ziel/Container/descendant::Signal satisfies ((not((some $Signal_Rahmen in //LST_Zustand_Ziel/Container/descendant::Signal_Rahmen satisfies (($Signal/Identitaet/Wert = //LST_Zustand_Ziel/Container/descendant::*[./Identitaet/Wert=($Signal_Rahmen/ID_Signal/Wert)]/Identitaet/Wert) and (some $Signal_Signalbegriff in //LST_Zustand_Ziel/Container/descendant::Signal_Signalbegriff satisfies (((//LST_Zustand_Ziel/Container/descendant::*[./Identitaet/Wert=($Signal_Signalbegriff/ID_Signal_Rahmen/Wert)]/Identitaet/Wert = $Signal_Rahmen/Identitaet/Wert) and (substring($Signal_Signalbegriff/Signalbegriff_ID/@xsi:type, 26) = 'Zs_3')) and (not(($Signal_Signalbegriff/Signal_Signalbegriff_Allg/Geschaltet/Wert='true'))))))))) or (not((some $Signal_Rahmen in //LST_Zustand_Ziel/Container/descendant::Signal_Rahmen satisfies (($Signal/Identitaet/Wert = //LST_Zustand_Ziel/Container/descendant::*[./Identitaet/Wert=($Signal_Rahmen/ID_Signal/Wert)]/Identitaet/Wert) and (some $Signal_Signalbegriff in //LST_Zustand_Ziel/Container/descendant::Signal_Signalbegriff satisfies (((//LST_Zustand_Ziel/Container/descendant::*[./Identitaet/Wert=($Signal_Signalbegriff/ID_Signal_Rahmen/Wert)]/Identitaet/Wert = $Signal_Rahmen/Identitaet/Wert) and (substring($Signal_Signalbegriff/Signalbegriff_ID/@xsi:type, 26) = 'Zs_3v')) and ($Signal_Signalbegriff/Signal_Signalbegriff_Allg/Geschaltet/Wert='true')))))))))"/>
        </rule>
    </pattern>
</schema>
```