package de.theminefighter.pp_verifier;

import de.theminefighter.pp_verifier.schematron.Validation;
import de.theminefighter.pp_verifier.synthesis.PpvlConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SystemTests {

    private final String zs33v = """
            ∀ Signal:
                    (∄ Signal_Rahmen:
                        Signal == Signal_Rahmen.ID_Signal ∧
                        ∃ Signal_Signalbegriff:
                            Signal_Signalbegriff.ID_Signal_Rahmen==Signal_Rahmen ∧
                            Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_3" ∧
                            ¬Signal_Signalbegriff.Signal_Signalbegriff_Allg.Geschaltet)
                    ∨ (∄ Signal_Rahmen:
                        Signal==Signal_Rahmen.ID_Signal ∧
                        ∃ Signal_Signalbegriff:
                            Signal_Signalbegriff.ID_Signal_Rahmen==Signal_Rahmen ∧
                            Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_3v" ∧
                            Signal_Signalbegriff.Signal_Signalbegriff_Allg.Geschaltet)
                        """;
    public static final File wrongTesthausen = new File("target/test-classes/ESTW-Testhausen_Zs3vF_60AA.planpro.xml");
    private final String zs13rule = """
            ZFahrw:={f: f ∈ Fstr_Fahrweg ∧ ∃ Fstr_Zug_Rangier:
                Fstr_Zug_Rangier.Fstr_Zug ≠ null ∧ Fstr_Zug_Rangier.ID_Fstr_Fahrweg==f}
            Stumpfgleise:= {s : s ∈ Signal ∧ "Zug_Ziel_ohne_Signal" ∈ s.Signal_Fiktiv.Fiktives_Signal_Funktion}
            Fw_stumpf:={f : f ∈ ZFahrw ∧ f.ID_Ziel ∈ Stumpfgleise}
            Start_nur_stumpf:={s: s ∈ Signal ∧ ∀ ZFahrw:
                ZFahrw.ID_Start ≠ s ∨ ZFahrw ∈ Fw_stumpf }
            Fweg_stumpf_ohne_ausn:={fw: fw ∈ Fw_stumpf ∧ fw.ID_Start ∉ Start_nur_stumpf}
            Zds:={s : s ∈ Signal ∧ s.Signal_Real.Signal_Funktion == "Zugdeckungssignal"}
            Fw_zds := {f: f ∈ ZFahrw ∧ f.ID_Ziel ∈ Zds}
            Kurzeinfahrten:={fw: fw ∈ ZFahrw ∧ (∃ Fstr_Signalisierung:
                Fstr_Signalisierung.ID_Fstr_Zug_Rangier.ID_Fstr_Fahrweg == fw ∧
                Fstr_Signalisierung.ID_Signal_Signalbegriff.Signalbegriff_ID.typ=="Ke" ∧
                Fstr_Signalisierung.ID_Signal_Signalbegriff_Ziel.ID_Signal_Rahmen.ID_Signal==fw.ID_Ziel ∧
                    sum({fwe.Begrenzung_B-fwe.Begrenzung_A: fwe ∈
                        Fstr_Signalisierung.ID_Fstr_Zug_Rangier.ID_Fstr_Fahrweg.Bereich_Objekt_Teilbereich})
                    >1,3*sum({fwe.Begrenzung_B-fwe.Begrenzung_A:
                        fwe ∈ fw.Bereich_Objekt_Teilbereich}))}
            Zs13_Soll:={f: f ∈ ZFahrw ∧
                (f ∈  Kurzeinfahrten ∨ f ∈ Fweg_stumpf_ohne_ausn ∨
                f ∈ Fw_zds)}
            Zs13prüfung ::= ∀ Fstr_Zug_Rangier: (Fstr_Zug_Rangier.ID_Fstr_Fahrweg ∈ Zs13_Soll) ==
            (∃ Fstr_Signalisierung:
            Fstr_Signalisierung.ID_Fstr_Zug_Rangier == Fstr_Zug_Rangier ∧
            Fstr_Signalisierung.ID_Signal_Signalbegriff.Signalbegriff_ID.typ=="Zs_13")
                        """;

    @Test
    public void zs3vcompile() throws Exception {
        String s = PpvlConverter.convertToSchematron(zs33v);
        Files.writeString(Path.of("testout.sch"), s);
    }
    @DisabledIfEnvironmentVariable(named = "NO_LONG_TESTS", matches = "1")
    @Test
    public void zs3vSucc() throws Exception {
        String s = PpvlConverter.convertToSchematron(zs33v);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
    @DisabledIfEnvironmentVariable(named = "NO_LONG_TESTS", matches = "1")
    @Test
    public void zs3vFail() throws Exception {
        String s = PpvlConverter.convertToSchematron(zs33v);
        Files.writeString(Path.of("testout.sch"), s);
        assertFalse(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),
                wrongTesthausen));
    }

    @Test
    public void realRuleZs8wZs6() throws Exception {
        String rule= """
                ∀ Signal_Rahmen:
                    (∃ Signal_Signalbegriff:
                    Signal_Signalbegriff.Signal_Signalbegriff_Allg.Geschaltet
                    ∧ Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_6"
                    ∧ Signal_Rahmen == Signal_Signalbegriff.ID_Signal_Rahmen)
                    ∨ (∄ Signal_Signalbegriff:
                    Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_8"
                    ∧ Signal_Rahmen == Signal_Signalbegriff.ID_Signal_Rahmen)
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertFalse(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),wrongTesthausen));
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
    @Test
    public void ultimateRuleCompile() throws Exception {
        String s = PpvlConverter.convertToSchematron(zs13rule);
        Files.writeString(Path.of("testout.sch"), s);
        //assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
    @DisabledIfEnvironmentVariable(named = "NO_LONG_TESTS", matches = "1")
    @Test
    public void ultimateRule() throws Exception {
        String s = PpvlConverter.convertToSchematron(zs13rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
}
