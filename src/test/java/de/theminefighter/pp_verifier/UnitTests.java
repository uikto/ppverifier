package de.theminefighter.pp_verifier;

import com.sun.xml.xsom.XSType;
import de.theminefighter.pp_verifier.XsdData.CommonTypes;
import de.theminefighter.pp_verifier.XsdData.SchemaInfo;
import de.theminefighter.pp_verifier.XsdData.Type;
import de.theminefighter.pp_verifier.synthesis.nodes.EqOperator;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class UnitTests {
    @Test
    public void testSchemaFiles() {
        File[] ppSchemas = SchemaInfo.getPpSchemas();
        assertNotNull(ppSchemas, "PPSchemas were not found, check working directory");
        assertEquals(29, ppSchemas.length);
    }

    @Test
    public void schemaInfo() throws Exception {
        XSType cLieferobjekt = SchemaInfo.getType("http://www.plan-pro.org/modell/Basisobjekte/1.10.0.1", "CLieferobjekt");
        Map<String, Type> cAnhang = SchemaInfo.getAttributesOf(cLieferobjekt);
        Type t1 = cAnhang.get("LO_Ersatz");
        assertFalse(t1.isSet());
        Type t2 = cAnhang.get("ID_LO_Einbau");
        assertTrue(t2.isSet());
    }

    @Test
    public void testInheritance() {
        assertTrue(SchemaInfo.isDerivedFromAlt(CommonTypes.guid, SchemaInfo.string));
        assertTrue(SchemaInfo.isDerivedFromAlt(CommonTypes.guid, CommonTypes.guid));
        assertFalse(SchemaInfo.isDerivedFromAlt(SchemaInfo.string, CommonTypes.guid));
    }

    @Test
    public void testAllowedComparison() {
        assertTrue(EqOperator.isComparisonLegal(CommonTypes.guid, SchemaInfo.string));
        assertFalse(EqOperator.isComparisonLegal(CommonTypes.guid, SchemaInfo.decimal));//TCID_Anschluss_Element
        assertTrue(EqOperator.isComparisonLegal(CommonTypes.getPPType("Verweise","TCID_Ziel"),
                CommonTypes.getPPType("Verweise","TCID_PZB_Element_Bezugspunkt")));
        assertFalse(EqOperator.isComparisonLegal(CommonTypes.getPPType("Verweise","TCID_Ziel"),
                CommonTypes.getPPType("Verweise","TCID_Anschluss_Element")));
    }
}
