package de.theminefighter.pp_verifier;

import de.theminefighter.pp_verifier.schematron.Validation;
import de.theminefighter.pp_verifier.synthesis.PpvlConverter;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

public class IntegrationTests {
    @Test
    public void invalidSymbol() {
        assertThrows(SynthesisException.class, ()-> PpvlConverter.convertToSchematron("abc::=xyz"));
    }

    @Test
    public void basicVariableTest() throws Exception {
        String rule= """
                a:=1
                r::=a==1
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
    @Test
    public void zs3ExistingTest() throws Exception {
        String rule= """
                a::=∃ Signal_Signalbegriff:
                                Signal_Signalbegriff.Signalbegriff_ID.typ=="Zs_3"
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
    @Test
    public void wrongTypUsage() throws Exception {
        String rule= """
                a::=∃ Signal_Signalbegriff:
                                Signal_Signalbegriff.typ=="Zs_3"
                            """;
        assertThrows(SynthesisException.class, ()-> PpvlConverter.convertToSchematron(rule));
    }
    @Test
    public void quantifiedObjectEqualsItself() throws Exception {
        String rule= """
                a::=alle Signal_Signalbegriff:
                                Signal_Signalbegriff==Signal_Signalbegriff
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
    @Test
    public void quantifyOverInlineSet() throws Exception {
        String rule= """
                a::=∃ s ∈ {1,2}:
                              s>1
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }

    @Test
    public void zs3vNfExistsTest() throws Exception {
        String rule= """
                a::= ∃ Signal_Signalbegriff:
                            Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_3v" ∧
                            ¬Signal_Signalbegriff.Signal_Signalbegriff_Allg.Geschaltet
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }

    @Test
    public void realRulePPIntegrityTest() throws Exception {
        String rule= """
               ∀ Fstr_Signalisierung:
                         Fstr_Signalisierung.ID_Signal_Signalbegriff.Signalbegriff_ID.typ ∉
                             {"Ks_1"; "Ks_2"; "Sh_1"}
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }

    @Test
    public void realRuleFstrZielNotStart() throws Exception {
        String rule= """
               ∀ Fstr_Fahrweg: Fstr_Fahrweg.ID_Start ≠ Fstr_Fahrweg.ID_Ziel
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }


    @Test
    public void somewhatRealFstrFromToHs() throws Exception {
        String rule= """
                Hauptsignalarten:= {"Hauptsignal"; "Hauptsperrsignal"; "Hauptsperrsignal Ne 14 Ls"; "Mehrabschnittssignal"; "Mehrabschnittssperrsignal"}
                                
                Hauptsig_Fs ::= ∀ Fstr_Zug_Rangier: Fstr_Zug_Rangier.Fstr_Zug == null ∨ (
                    (∃ Signal:
                        Fstr_Zug_Rangier.ID_Fstr_Fahrweg.ID_Start == Signal ∧
                        Signal.Signal_Real.Signal_Real_Aktiv_Schirm.Signal_Art ∈  Hauptsignalarten)
                    ∧ ∃ Signal:
                        Fstr_Zug_Rangier.ID_Fstr_Fahrweg.ID_Ziel == Signal ∧
                        Signal.Signal_Real.Signal_Real_Aktiv_Schirm.Signal_Art ∈  Hauptsignalarten
                    )
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertFalse(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }

    @Test
    public void zs8existsTest() throws Exception {
        String rule= """
                a::= ∃ Signal_Signalbegriff:
                            Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_8"
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertFalse(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),SystemTests.wrongTesthausen));
    }
    @Test
    public void zs3FexitsTest() throws Exception {
        String rule= """
                a::= ∃ Signal_Signalbegriff:
                            Signal_Signalbegriff.Signalbegriff_ID.typ == "Zs_3" ∧
                            Signal_Signalbegriff.Signal_Signalbegriff_Allg.Geschaltet
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
    @Test
    public void atomNullTest() throws Exception {
        String rule= """
                a::=1!=null
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }

    @Test
    public void umlautTest() throws Exception {
        String rule= """
                ü:=1
                a::=2!=ü
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }

    @Test
    public void refResolutionTest() throws  Exception{
        String rule= """
            ZFahrw:=Fstr_Fahrweg
            Stumpfgleise:= Signal
            Fw_stumpf:= {f : f ∈ ZFahrw ∧ f.ID_Ziel ∈ Stumpfgleise}
            ab::=len(Fw_stumpf)>0
                            """;
        String s = PpvlConverter.convertToSchematron(rule);
        Files.writeString(Path.of("testout.sch"), s);
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),new File(Main.testhausenPath)));
    }
    @Disabled
    @Test
    public void debugTest() throws Exception {
        assertTrue(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"), new File(Main.testhausenPath)));
        assertFalse(Validation.validateXMLViaXSLTSchematron(new File("testout.sch"),
                new File("target/test-classes/ESTW-Testhausen_Zs3vF_60AA.planpro.xml")));
    }
}
