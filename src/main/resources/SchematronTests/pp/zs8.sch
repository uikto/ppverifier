<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <pattern>
        <rule context="Signal_Rahmen">
            <assert test="
//LST_Zustand_Ziel/Container/Signal_Signalbegriff
[Signal_Signalbegriff_Allg/Geschaltet/Wert='true']
[Signalbegriff_ID/@xsi:type='nsSignalbegriffe_Ril_301:Zs_6']
[ID_Signal_Rahmen/Wert=(current()/Identitaet/Wert)]
or
not(
//LST_Zustand_Ziel/Container/Signal_Signalbegriff
[Signalbegriff_ID/@xsi:type='nsSignalbegriffe_Ril_301:Zs_8']
[ID_Signal_Rahmen/Wert=(current()/Identitaet/Wert)]
)
"/>
        </rule>
    </pattern>
</schema>
        <!--

        //LST_Zustand_Ziel/Container/Signal_Signalbegriff
        [Signal_Signalbegriff_Allg/Geschaltet/Wert='true']
        [Signalbegriff_ID/@xsi:type='nsSignalbegriffe_Ril_301:Zs_6']
        [ID_Signal_Rahmen/Wert=(current()/Identitaet/Wert)]

        or
        not(
        //LST_Zustand_Ziel/Container/Signal_Signalbegriff
        [Signalbegriff_ID/@xsi:type='nsSignalbegriffe_Ril_301:Zs_8']
        [ID_Signal_Rahmen/Wert=(current()/Identitaet/Wert)]
        )
                  -->