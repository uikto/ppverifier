
grammar PpVerifierDsl;
@header {
package de.theminefighter.pp_verifier.parser;
}
WS: [ \n\t\r]+ -> skip;
OP_EQ: '==' ;
OP_NEQ: '!=' | '≠';
OP_AND: '&&' | '&' | '∧';
OP_OR: '||' | '|' | '∨';
OP_NEG: '!' | '¬';
OP_NOTIN : '∉';
OP_IN : 'in' | '∈';
OP_EXISTS : 'existiert' | '∃';
OP_NOTEXISTS : '∄';
OP_ALL : 'alle' | '∀';
OP_LEQ : '<=' | '≤';
OP_GEQ :  '>=' | '≥';
OP_LT: '<';
OP_GT: '>';
OP_MUL: '*';
OP_ADD: '+';
OP_SUB: '-';
OP_DIV: '/';

TOK_VAR_ASSIGNMENT: ':=';
TOK_RULE_ASSIGNMENT: '::=';
TOK_COLON:':';
TOK_LBR:'(';
TOK_RBR:')';
TOK_DOT:'.';
TOK_SEMICOLON:';';
TOK_LCBR:'{';
TOK_RCBR:'}';
TOK_NULL:'null';

StringLiteral : '"' [A-ZÄÖÜa-zäöüß0-9\-_ .]+ '"';
NumericLiteral : [0-9]+ ([.,] [0-9]+)?;
Ident : [A-ZÄÖÜa-zäöüß_]+[A-ZÄÖÜa-zäöüß_0-9]*;
ppvl_doc: expr #singleRuleDoc| statement+ #fullDoc;
expr: exp1;
path : Ident  (TOK_DOT Ident)*;

statement: varDef | ruleDef;
varDef : Ident TOK_VAR_ASSIGNMENT expr;
ruleDef : Ident TOK_RULE_ASSIGNMENT expr;

ops_eq_neq: OP_EQ | OP_NEQ;
ops_REL : OP_LEQ | OP_GEQ | OP_LT | OP_GT;
ops_QUANT : OP_EXISTS | OP_NOTEXISTS | OP_ALL;

exp8 : TOK_LBR exp1 TOK_RBR #braced_expr
	 | path #path_exp
	 | StringLiteral #str_literal_expr
	 | (OP_SUB)? NumericLiteral #num_literal_expr
	 | TOK_LCBR exp1 TOK_COLON Ident OP_IN path (OP_AND exp4)? TOK_RCBR #set_expr
	 | (ops_QUANT Ident (OP_IN source=exp1)? TOK_COLON condition=exp1) #quant_Expr
	 | Ident TOK_LBR exp1 TOK_RBR #func_call
	 | TOK_LCBR exp1 (TOK_SEMICOLON exp1)* TOK_RCBR #set_by_elements
	 ;
exp7 : exp8 #exp_sh7
     | OP_NEG exp8 #negate
     ;
exp6 : exp7 #exp_sh6
 	 | exp6 op=(OP_MUL | OP_DIV) exp7 #exp_mul_div
 	 ;
exp5 : exp6 #exp_sh5
 	 | exp5 op=(OP_ADD | OP_SUB) exp6 #exp_add_sub
 	 ;
exp5OrNull: exp5 #exp5_no_null
	 | TOK_NULL #exp_null
	 ;
exp4 : exp5 #exp_sh4
 	 | exp5OrNull (ops_REL | ops_eq_neq) exp5OrNull #exp_rel_eq
 	 | exp5OrNull op=(OP_IN | OP_NOTIN) exp5 #exp_in
 	 ;
exp3 : exp4 #exp_sh3
	 | exp3 OP_AND exp4 #exp_and;
exp2 : exp3 #exp_sh2
	 | exp2 OP_OR exp3 #exp_or;
exp1 : exp2 #exp_sh1;