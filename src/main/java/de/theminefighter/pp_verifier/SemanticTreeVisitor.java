package de.theminefighter.pp_verifier;

import de.theminefighter.pp_verifier.parser.PpVerifierDslBaseVisitor;
import de.theminefighter.pp_verifier.parser.PpVerifierDslParser;
import de.theminefighter.pp_verifier.synthesis.Helpers;
import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.nodes.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.List;

/**
 * Visitor for PPvl Expressions, returning the equivalent Semantic Tree
 */
public class SemanticTreeVisitor extends PpVerifierDslBaseVisitor<SemanticTreeNode> {
    @Override
    public SemanticTreeNode visitSet_by_elements(PpVerifierDslParser.Set_by_elementsContext ctx) {
        return new SetByElements(ctx.exp1().stream().map(this::visit).toList());
    }

    @Override
    public SemanticTreeNode visitFunc_call(PpVerifierDslParser.Func_callContext ctx) {
        return new FunctionCall(visit(ctx.exp1()), ctx.Ident().getText());
    }

    @Override
    public SemanticTreeNode visitBraced_expr(PpVerifierDslParser.Braced_exprContext ctx) {
        return visit(ctx.exp1());
    }

    @Override
    public SemanticTreeNode visitPath_exp(PpVerifierDslParser.Path_expContext ctx) {
        return super.visitPath_exp(ctx);
    }

    @Override
    public Path visitPath(PpVerifierDslParser.PathContext ctx) {
        return new Path(ctx.Ident().stream().map(ParseTree::getText).toList());

    }

    @Override
    public SemanticTreeNode visitStr_literal_expr(PpVerifierDslParser.Str_literal_exprContext ctx) {
        String literal = ctx.StringLiteral().getText();
        return new StringLiteral(literal.substring(1, literal.length() - 1));
    }

    @Override
    public SemanticTreeNode visitNum_literal_expr(PpVerifierDslParser.Num_literal_exprContext ctx) {
        return new NumericLiteral(Float.parseFloat(ctx.getText().replace(',', '.')));
    }

    @Override
    public SemanticTreeNode visitSet_expr(PpVerifierDslParser.Set_exprContext ctx) {
        return new SetModification(visit(ctx.exp1()), ctx.Ident().getText(), visitPath(ctx.path()),
                ctx.exp4() == null ? null : visit(ctx.exp4()));
    }

    @Override
    public SemanticTreeNode visitNegate(PpVerifierDslParser.NegateContext ctx) {
        return new NegateOperator(visit(ctx.exp8()));
    }

    @Override
    public SemanticTreeNode visitExp_mul_div(PpVerifierDslParser.Exp_mul_divContext ctx) {
        return new ArithmeticOperator(visit(ctx.exp6()), visit(ctx.exp7()), ctx.op.getText().charAt(0));
    }

    @Override
    public SemanticTreeNode visitExp_add_sub(PpVerifierDslParser.Exp_add_subContext ctx) {
        return new ArithmeticOperator(visit(ctx.exp5()), visit(ctx.exp6()), ctx.op.getText().charAt(0));
    }

    @Override
    public SemanticTreeNode visitExp_rel_eq(PpVerifierDslParser.Exp_rel_eqContext ctx) {
        SemanticTreeNode result;
        SemanticTreeNode op1 = visit(ctx.exp5OrNull(0));
        SemanticTreeNode op2 = visit(ctx.exp5OrNull(1));
        if (ctx.ops_eq_neq() != null) {
            result = new EqOperator(op1, op2);
            if (ctx.ops_eq_neq().OP_NEQ() != null) {
                result = new NegateOperator(result);
            }
        } else {
            PpVerifierDslParser.Ops_RELContext opsRelContext = ctx.ops_REL();
            boolean nonStrict = opsRelContext.OP_GEQ() != null || opsRelContext.OP_LEQ() != null;
            boolean less = opsRelContext.OP_LT() != null || opsRelContext.OP_LEQ() != null;
            if (less ^ nonStrict) {
                SemanticTreeNode tmp = op1;
                op1 = op2;
                op2 = tmp;
            }
            result = new GreaterOperator(op1, op2);
            if (nonStrict) {
                result = new NegateOperator(result);
            }
        }
        return result;
    }

    @Override
    public SemanticTreeNode visitExp_in(PpVerifierDslParser.Exp_inContext ctx) {
        SemanticTreeNode element = visit(ctx.exp5OrNull());
        var tempName = Helpers.getTempName();
        SemanticTreeNode result = new Quantifier(new EqOperator(element, new Path(List.of(tempName))), false,visit(ctx.exp5()), tempName );
        if (ctx.OP_NOTIN() != null) result = new NegateOperator(result);
        return result;
    }

    @Override
    public SemanticTreeNode visitExp_and(PpVerifierDslParser.Exp_andContext ctx) {
        return new LogicOperator(visit(ctx.exp3()), true, visit(ctx.exp4()));
    }

    @Override
    public SemanticTreeNode visitExp_or(PpVerifierDslParser.Exp_orContext ctx) {
        return new LogicOperator(visit(ctx.exp2()), false, visit(ctx.exp3()));
    }

    @Override
    public SemanticTreeNode visitQuant_Expr(PpVerifierDslParser.Quant_ExprContext ctx) {
        String quantifiedIdent = ctx.Ident().getText();
        SemanticTreeNode source = ctx.source == null ?
                new Path(List.of(new String[]{quantifiedIdent}))
                : visit(ctx.source);
        SemanticTreeNode result = new Quantifier(visit(ctx.condition), ctx.ops_QUANT().OP_ALL() != null, source, quantifiedIdent);
        if (ctx.ops_QUANT().OP_NOTEXISTS() != null) result = new NegateOperator(result);
        return result;
    }
}
