package de.theminefighter.pp_verifier.synthesis;

import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class Helpers {
    private static final Random random = new Random();

    /**
     * Generates variable names
     * @return a name of a variable valid in XPath 2.0 and containing at least 64 bits of entropy to avoid collisions
     */
    @NotNull
    public static String getTempName() {
        return "a"+Long.toHexString(random.nextLong());
    }
}
