package de.theminefighter.pp_verifier.synthesis;

/**
 * Any exception during the Schematron Synthesis due to an invalid ppvl doc is a synthesis exception
 */
public class SynthesisException extends RuntimeException {
    public SynthesisException() {
        super();
    }

    public SynthesisException(String message) {
        super(message);
    }

    public SynthesisException(String message, Throwable cause) {
        super(message, cause);
    }

    public SynthesisException(Throwable cause) {
        super(cause);
    }

    protected SynthesisException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
