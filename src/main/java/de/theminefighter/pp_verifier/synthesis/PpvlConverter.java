package de.theminefighter.pp_verifier.synthesis;

import de.theminefighter.pp_verifier.SchematronVisitor;
import de.theminefighter.pp_verifier.parse_helpers.AbortingErrorListener;
import de.theminefighter.pp_verifier.parser.PpVerifierDslLexer;
import de.theminefighter.pp_verifier.parser.PpVerifierDslParser;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

public class PpvlConverter {
    /**
     * Converts a given PPVL Document to Schematron
     * @param parseContent a valid ppvl document
     * @return an equivalent schematron document with Xpath 2.0
     */
    public static String convertToSchematron(String parseContent) {
        var lx = new PpVerifierDslLexer(CharStreams.fromString(parseContent));
        lx.addErrorListener(AbortingErrorListener.Singleton);
        CommonTokenStream cts = new CommonTokenStream(lx);
        var parser = new PpVerifierDslParser(cts);
        parser.setErrorHandler(new BailErrorStrategy());
        parser.addErrorListener(AbortingErrorListener.Singleton);
        PpVerifierDslParser.Ppvl_docContext pt = parser.ppvl_doc();
        if (!cts.getTokens().getLast().getText().equals("<EOF>"))
            throw new SynthesisException("Input was not parsed fully");
        String rules = new SchematronVisitor().visit(pt);
        return ("""
                <?xml version="1.0" encoding="UTF-8"?>
                <schema xmlns="http://purl.oclc.org/dsdl/schematron">
                    <ns uri="http://www.plan-pro.org/modell/PlanPro/1.10.0.1" prefix="nsPlanPro"/>
                    <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
                    <ns uri="http://www.w3.org/2005/xpath-functions" prefix="fn"/>
                    <pattern>
                        <rule context="LST_Planung">
                            %s
                        </rule>
                    </pattern>
                </schema>""").formatted(rules);
    }
}
