package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import de.theminefighter.pp_verifier.XsdData.Type;

/**
 * A logic operator in PPVl
 * and field might be removed at some point and replaced bt using more negates when necessary
 */
public class LogicOperator extends SemanticTreeNode {
    private final SemanticTreeNode op1;
    private final boolean and;
    private final SemanticTreeNode op2;

    public LogicOperator(SemanticTreeNode op1, boolean and, SemanticTreeNode op2) {
        this.op1 = op1;
        this.and = and;
        this.op2 = op2;
    }

    @Override
    public String getXPath(SymbolTable st) {
        return String.format("(%s %s %s)", op1.getXPath(st), and ? "and" : "or", op2.getXPath(st));
    }

    @Override
    public Type getExpType(SymbolTable st) {
        if (!op1.getExpType(st).isSingleBool() || !op2.getExpType(st).isSingleBool())
            throw new SynthesisException("Logical operator applied to non bool type");
        return Type.singleBool;
    }
}
