package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.XsdData.CommonTypes;
import de.theminefighter.pp_verifier.XsdData.SchemaInfo;
import de.theminefighter.pp_verifier.XsdData.Type;
import de.theminefighter.pp_verifier.synthesis.Helpers;
import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;

import java.util.ArrayList;
import java.util.List;

/**
 * A path in Ppvl with a lot of edge case handling
 */
public class Path extends SemanticTreeNode {
    private final String startElement;
    private final List<String> furtherPathElements;
    private static final String validationTarget = "//LST_Zustand_Ziel/Container/descendant::";
    private static final String nameOfTypeAttr = "typ";

    private String xpath;

    private Type tp;

    public Path(List<String> pathElements) {
        if (pathElements.isEmpty()) throw new RuntimeException("Paths without contents are disallowed");
        pathElements = new ArrayList<>(pathElements);
        startElement = pathElements.removeFirst();
        furtherPathElements = pathElements;
    }

    public void Load(SymbolTable st) {
        if (xpath != null)
            return;
        if (st.containsKey(startElement)) {
            xpath = st.get(startElement).schematronName;
            tp = st.get(startElement).type;
        } else {
            xpath = validationTarget + startElement;
            tp = new Type(SchemaInfo.getTypeFromName(startElement), true);
        }
        handleImplicitRefsAndWert();
        for (String pathElem : furtherPathElements) {
            if (nameOfTypeAttr.equals(pathElem)) {
                String SigBgr_IDstr = SchemaInfo.TypeToStr(CommonTypes.getPPType("Signalbegriffe_Struktur", "TCSignalbegriff_ID"));
                if (!SigBgr_IDstr.equals(SchemaInfo.TypeToStr(tp.xmlType())))
                    throw new SynthesisException(".typ applied to object that is not TCSignalbegriff_ID");
                //substring-after is broken
                //"substring-after(':',%s/@xsi:type)"
                xpath = "substring(%s/@xsi:type, 26)".formatted(xpath);
                tp = new Type(SchemaInfo.string, tp.isSet());
                continue;
            }
            xpath += "/" + pathElem;
            tp = tp.getTypeFromAttr(pathElem);
            handleImplicitRefsAndWert();
        }
    }

    private void handleImplicitRefsAndWert() {
        String typeName = SchemaInfo.TypeToStr(tp.xmlType());
        if (SchemaInfo.implicitResolve.containsKey(typeName)) {
            Type nextTp = tp.withXmlType(SchemaInfo.implicitResolve.get(typeName));
            if (nextTp.xmlType() == null)
                throw new SynthesisException("issue");
            tp = nextTp;
            if (tp.isSet()) {
                var varname = Helpers.getTempName();
                xpath = "for $%s in (%s) return (%s*[./Identitaet/Wert=(%s/Wert)])"
                        .formatted(varname, xpath, validationTarget, varname);
            } else {
                xpath = "%s*[./Identitaet/Wert=(%s/Wert)]".formatted(validationTarget, xpath);
            }
            return;
        }
        if (SchemaInfo.isDerivedFromAlt(tp.xmlType(), CommonTypes.cBasisAttribut) && !SchemaInfo.isDerivedFromAlt(tp.xmlType(), CommonTypes.tcZeiger )) {
            xpath += "/Wert";
            tp = SchemaInfo.getAttributesOf(tp.xmlType()).get("Wert").orSet(tp.isSet());
        }
        if (SchemaInfo.isDerivedFromAlt(tp.xmlType(), SchemaInfo.bool))
            xpath= "(%s='true')".formatted(xpath);
    }

    @Override
    public String getXPath(SymbolTable st) {
        Load(st);
        return xpath;
    }

    @Override
    public Type getExpType(SymbolTable st) {
        Load(st);
        return tp;
    }

}
