package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.XsdData.Type;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import de.theminefighter.pp_verifier.synthesis.Variable;

/**
 * A Quantifier in PPVl
 * forall might be removed at some point and replaced by more negations
 */
public class Quantifier extends SemanticTreeNode {

    public final SemanticTreeNode conditional;

    public final boolean forall;

    public final SemanticTreeNode setSource;

    public final String quantifiedIdentifier;

    public Quantifier(SemanticTreeNode conditional, boolean forall, SemanticTreeNode setSource, String quantifiedIdentifier) {
        this.conditional = conditional;
        this.forall = forall;
        this.setSource = setSource;
        this.quantifiedIdentifier = quantifiedIdentifier;
    }

    @Override
    public String getXPath(SymbolTable st) {
        String fstr= forall? "(every %s in %s satisfies %s)" :"(some %s in %s satisfies %s)";
        String conditionalStr = conditional.getXPath(expandSymbolTable(st));
        return String.format(fstr,'$'+quantifiedIdentifier, setSource.getXPath(st), conditionalStr);

    }

    @Override
    public Type getExpType(SymbolTable st) {
        SymbolTable condSt = expandSymbolTable(st);
        if (!conditional.getExpType(condSt).isSingleBool())
            throw new SynthesisException("Quantified need a function returning a bool");
        return Type.singleBool;
    }

    private SymbolTable expandSymbolTable(SymbolTable st) {
        Type sourceSet= setSource.getExpType(st);
        if (!sourceSet.isSet()) throw new SynthesisException("You can't quantify over a non set");
        Variable quantifiedVar= new Variable('$'+quantifiedIdentifier, new Type(sourceSet.xmlType(), false));
        SymbolTable condSt = st.makeCopy();
        condSt.put(quantifiedIdentifier,quantifiedVar);
        return condSt;
    }
}
