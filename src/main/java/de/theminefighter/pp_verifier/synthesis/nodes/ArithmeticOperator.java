package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import de.theminefighter.pp_verifier.XsdData.Type;

/**
 * Node for the arithmetic operations /+-*
 */
public class ArithmeticOperator extends SemanticTreeNode {

    private final SemanticTreeNode op1;
    private final SemanticTreeNode op2;
    private final char opChar;

    private static final String allowedChars="*/-+";

    public ArithmeticOperator(SemanticTreeNode op1, SemanticTreeNode op2, char opChar) {
        this.op1 = op1;
        this.op2 = op2;
        if (allowedChars.indexOf(opChar)==-1)
            throw new RuntimeException("Arithmetic Operator with invalid op created");
        this.opChar = opChar;
    }

    @Override
    public String getXPath(SymbolTable st) {
        return String.format("(%s %s %s)", op1.getXPath(st), opChar, op2.getXPath(st));
    }

    @Override
    public Type getExpType(SymbolTable st) {
        if (!op1.getExpType(st).isSingleNum() || !op2.getExpType(st).isSingleNum())
            throw new SynthesisException("Logical operator applied to non bool type");
        return Type.singleNum;
    }
}
