package de.theminefighter.pp_verifier.synthesis.nodes;

import com.sun.istack.Nullable;
import de.theminefighter.pp_verifier.XsdData.SchemaInfo;
import de.theminefighter.pp_verifier.XsdData.Type;
import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import de.theminefighter.pp_verifier.synthesis.Variable;

/**
 * An operation which runs a map and optionally a filter on a set; might be seperated in these two and some point in time
 */
public class SetModification extends SemanticTreeNode {
    private final SemanticTreeNode mapExpr;
    private final String identifierInExpr;
    private final Path sourceSet;
    @Nullable
    private final SemanticTreeNode filterExpr;
    private final String schematronIdent;

    public SetModification(SemanticTreeNode mapExpr, String identifierInExpr, Path sourceSet, SemanticTreeNode filterExpr) {
        this.mapExpr = mapExpr;
        this.identifierInExpr = identifierInExpr;
        this.sourceSet = sourceSet;
        this.filterExpr = filterExpr;
        schematronIdent = "$" + identifierInExpr;
    }

    @Override
    public String getXPath(SymbolTable st) {
        SymbolTable expandedSt = makeSt(st);
        String mapString = mapExpr.getXPath(expandedSt);
        if (filterExpr!=null) {
            mapString= String.format("(if %s then %s else ())", filterExpr.getXPath(expandedSt), schematronIdent);
        }
        return String.format("for %s in %s return %s", schematronIdent, sourceSet.getXPath(st), mapString);
    }

    @Override
    public Type getExpType(SymbolTable st) {
        Type sourceSetType= sourceSet.getExpType(st);
        if (!sourceSetType.isSet())
            throw new SynthesisException("You can not create a set from a source which is not a set!");
        SymbolTable symbolTableWithVar = makeSt(st);
        Type mappedType = mapExpr.getExpType(symbolTableWithVar);
        if (filterExpr!=null) {
            Type filterType = filterExpr.getExpType(symbolTableWithVar);
            if (filterType.isSet() || !filterType.xmlType().getName().equals("boolean") ||
                    !filterType.xmlType().getTargetNamespace().equals(SchemaInfo.xmlSchemaLocation))
                throw new SynthesisException("Filter expressions must return a single boolean");
        }
        return mappedType.orSet(true);
    }

    private SymbolTable makeSt(SymbolTable source) {
        SymbolTable result = source.makeCopy();
        Type sourceSetEntryType = new Type(sourceSet.getExpType(source).xmlType(), false);
        Variable setVar = new Variable(schematronIdent, sourceSetEntryType);
        result.put(identifierInExpr, setVar);
        return result;
    }

}
