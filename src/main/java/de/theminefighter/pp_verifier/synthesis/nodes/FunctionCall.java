package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.XsdData.SchemaInfo;
import de.theminefighter.pp_verifier.XsdData.Type;
import de.theminefighter.pp_verifier.synthesis.Function;
import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;

public class FunctionCall extends SemanticTreeNode {
    private final SemanticTreeNode parameter;

    private final String name;

    public FunctionCall(SemanticTreeNode parameter, String name){
        this.parameter = parameter;
        this.name = name;
    }
    @Override
    public String getXPath(SymbolTable st) {
        if (!Function.defaultFuns.containsKey(name))
            throw new SynthesisException("Function unknown: "+ name);
        return String.format("%s(%s)", Function.defaultFuns.get(name).xpath(), parameter.getXPath(st));
    }

    @Override
    public Type getExpType(SymbolTable st) {
        if (!Function.defaultFuns.containsKey(name))
            throw new SynthesisException("Function unknown: "+ name);
        Function fun=  Function.defaultFuns.get(name);
        Type param_i= parameter.getExpType(st);
        Type param_s= fun.param();
        if (param_s.isSet()!= param_i.isSet())
            throw new SynthesisException("Function parameter must be a set if and only if the function requires it: "+ name);
        if (!SchemaInfo.isDerivedFromAlt(param_i.xmlType(), param_s.xmlType()))
            throw new SynthesisException("Function parameter supplied is not derived from the type required: "+ name);
        return fun.returnType();
    }
}
