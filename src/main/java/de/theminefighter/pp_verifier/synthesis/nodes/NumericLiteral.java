package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.XsdData.Type;

/**
 * A numeric literal in ppvl
 */
public class NumericLiteral extends SemanticTreeNode {
    private final float content;

    public NumericLiteral(float content) {
        this.content = content;
    }

    @Override
    public String getXPath(SymbolTable st) {
        return String.valueOf(content);
    }

    @Override
    public Type getExpType(SymbolTable st) {
        return Type.singleNum;
    }
}
