package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import de.theminefighter.pp_verifier.XsdData.Type;

/**
 * A greater than operator, only applicable to numeric types
 */
public class GreaterOperator extends SemanticTreeNode {
    private final SemanticTreeNode op1;
    private final SemanticTreeNode op2;

    public GreaterOperator(SemanticTreeNode op1, SemanticTreeNode op2) {
        this.op1 = op1;
        this.op2 = op2;
    }

    @Override
    public String getXPath(SymbolTable st) {
        return String.format("(%s > %s)", op1.getXPath(st), op2.getXPath(st));
    }

    @Override
    public Type getExpType(SymbolTable st) {
        if (!op1.getExpType(st).isSingleNum() || !op2.getExpType(st).isSingleNum())
            throw new SynthesisException("Logical operator applied to non bool type");
        return Type.singleBool;
    }


}
