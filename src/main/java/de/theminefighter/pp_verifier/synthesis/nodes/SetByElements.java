package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.XsdData.SchemaInfo;
import de.theminefighter.pp_verifier.XsdData.Type;
import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;

import java.util.Collection;
import java.util.List;

/**
 * Node to create a set from a list of elements
 */
public class SetByElements extends SemanticTreeNode {
    private final Collection<SemanticTreeNode> elements;

    public SetByElements(Collection<SemanticTreeNode> elements) {
        if (elements.isEmpty()) throw new RuntimeException("Empty sets are illegal");
        this.elements = elements;
    }

    @Override
    public String getXPath(SymbolTable st) {
        return "("+String.join(",",elements.stream().map(x->x.getXPath(st)).toList())+")";
    }

    @Override
    public Type getExpType(SymbolTable st) {
        List<Type> types = elements.stream().map(x -> x.getExpType(st)).toList();
        if (types.stream().allMatch(Type::isSingleNum)) return Type.singleNum.orSet(true);
        if (types.stream().anyMatch(Type::isSet)) throw new SynthesisException("Elements of set should not be types");
        var setType=types.getFirst().xmlType();
        if (types.stream().allMatch(x-> SchemaInfo.isTypeEqual(x.xmlType(), setType))) return new Type(setType, true);
        throw new SynthesisException("Types of set are not equal");
    }
}
