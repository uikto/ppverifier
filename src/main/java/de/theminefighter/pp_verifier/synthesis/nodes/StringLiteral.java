package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.XsdData.Type;

/**
 * A string literal node
 */
public class StringLiteral extends SemanticTreeNode {
    private final String content;

    public StringLiteral(String content) {
        this.content = content;
    }

    @Override
    public String getXPath(SymbolTable st) {
        return "'" + content + "'";
    }

    @Override
    public Type getExpType(SymbolTable st) {
        return Type.singleStr;
    }
}
