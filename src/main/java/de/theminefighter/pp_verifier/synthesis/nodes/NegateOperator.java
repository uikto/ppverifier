package de.theminefighter.pp_verifier.synthesis.nodes;

import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import de.theminefighter.pp_verifier.XsdData.Type;

/**
 * A simple boolean negation in ppvl
 */
public class NegateOperator extends SemanticTreeNode {
    private final SemanticTreeNode content;

    public NegateOperator(SemanticTreeNode content) {
        this.content = content;
    }

    @Override
    public String getXPath(SymbolTable st) {
        return String.format("(not(%s))", content.getXPath(st));
    }

    @Override
    public Type getExpType(SymbolTable st) {
        if (!content.getExpType(st).isSingleBool())
            throw new SynthesisException("Negate operator applied to non bool type");
        return Type.singleBool;
    }
}
