package de.theminefighter.pp_verifier.synthesis.nodes;

import com.sun.xml.xsom.XSType;
import de.theminefighter.pp_verifier.XsdData.CommonTypes;
import de.theminefighter.pp_verifier.XsdData.SchemaInfo;
import de.theminefighter.pp_verifier.XsdData.Type;
import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.VisibleForTesting;

import java.util.Collections;

/**
 * The PPVl equality operator
 */
public class EqOperator extends SemanticTreeNode {

    @Nullable
    private final SemanticTreeNode op1;
    @Nullable
    private final SemanticTreeNode op2;

    public EqOperator(@Nullable SemanticTreeNode op1, @Nullable SemanticTreeNode op2) {
        if (op1 == null) {
            op1 = op2;
            op2 = null;
        }
        this.op1 = op1;
        this.op2 = op2;
        assert (op1 == null && op2 == null) == (op1 == null);
    }

    private static String getOpString(SemanticTreeNode op, SymbolTable st) {
        Type t = op.getExpType(st);
        String basicXPath = op.getXPath(st);
        if (t.isSet()) return basicXPath;
        if (SchemaInfo.isDerivedFromAlt(t.xmlType(), CommonTypes.tcZeiger)) return basicXPath+"/Wert";
        if (SchemaInfo.isDerivedFromAlt(t.xmlType(), CommonTypes.cUrObjekt)) return basicXPath + "/Identitaet/Wert";
        return basicXPath;
    }

    private static Type getOpType(Type t) {
        if (t.isSet()) return t;
        if (SchemaInfo.isDerivedFromAlt(t.xmlType(), CommonTypes.cUrObjekt))
            return new Type(CommonTypes.guid, false);
        return t;
    }

    @Override
    public String getXPath(SymbolTable st) {
        assert (op1 == null && op2 == null) == (op1 == null);
        if (op1 == null) return "true";
        String op1Str = getOpString(op1, st);
        if (op2 == null) {
            if (SchemaInfo.isDerivedFromAlt(op1.getExpType(st).xmlType(), SchemaInfo.anyAtom))
                return String.format("not(exists(%s))", op1Str);
            return String.format("(%s /@xsi:null='true' or not(exists(%s)))", op1Str, op1Str);}
        if (SchemaInfo.isDerivedFromAlt(getOpType(op1.getExpType(st)).xmlType(),SchemaInfo.anyAtom) ||
                SchemaInfo.isDerivedFromAlt(getOpType(op1.getExpType(st)).xmlType(), CommonTypes.tcZeiger)) {
            return String.format("(%s = %s)", op1Str, getOpString(op2, st));
        } else {
            return String.format("(%s is %s)", op1Str, getOpString(op2, st));
        }
    }

    @Override
    public Type getExpType(SymbolTable st) {
        assert (op1 == null && op2 == null) == (op1 == null);
        if (op1 == null) return Type.singleBool;
        if (op2 == null) {
            if (getOpType(op1.getExpType(st)).isSet())
                throw new SynthesisException("Sets can not be compared to null");
            return Type.singleBool;
        }
        Type t1 = op1.getExpType(st);
        Type t2 = op2.getExpType(st);
        if (!isComparisonLegal(t1, t2))
            throw new SynthesisException("These types can not be compared:" + t1 + ", " + t2);
        return Type.singleBool;
    }

    @Contract(pure = true)
    private static boolean isComparisonLegal(Type t1, Type t2) {
        return !t1.isSet() && !t2.isSet() && isComparisonLegal(t1.xmlType(), t2.xmlType());
    }

    @VisibleForTesting
    @Contract(pure = true)
    public static boolean isComparisonLegal(XSType t1, XSType t2) {
        if (SchemaInfo.isDerivedFromAlt(t1, SchemaInfo.decimal) &&
                SchemaInfo.isDerivedFromAlt(t2, SchemaInfo.decimal))
            return true;
        for (XSType pt1: SchemaInfo.refTypes.getOrDefault(SchemaInfo.TypeToStr(t1), Collections.singleton(t1))){
            for (XSType pt2: SchemaInfo.refTypes.getOrDefault(SchemaInfo.TypeToStr(t2), Collections.singleton(t2))){
                if (SchemaInfo.isDerivedFromAlt(pt1, pt2) || SchemaInfo.isDerivedFromAlt(pt2, pt1))
                    return true;
            }
        }
        return false;
    }
}
