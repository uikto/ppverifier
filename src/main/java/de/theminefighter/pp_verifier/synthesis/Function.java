package de.theminefighter.pp_verifier.synthesis;

import de.theminefighter.pp_verifier.XsdData.SchemaInfo;
import de.theminefighter.pp_verifier.XsdData.Type;

import java.util.Map;

/**
 *
 * @param xpath the xpath code for executing that function
 * @param returnType the type the function returns
 * @param param the type of the parameter of the function
 */
public record Function(String xpath, Type returnType, Type param) {
    private static final Type numSet= new Type(SchemaInfo.decimal, true);
    public static final Map<String, Function> defaultFuns= Map.of(
            "min", new Function("fn:min", Type.singleNum, numSet),
            "max", new Function("fn:max", Type.singleNum, numSet),
            "sum", new Function("fn:sum", Type.singleNum, numSet),
            "avg", new Function("fn:avg", Type.singleNum, numSet),
            "len", new Function("fn:count", Type.singleNum, new Type(SchemaInfo.any, true))
    );

}
