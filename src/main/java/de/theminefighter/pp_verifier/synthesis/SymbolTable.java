package de.theminefighter.pp_verifier.synthesis;

import java.util.HashMap;

/**
 * A PPVl Symbol Table Mapping names to variables
 */
public class SymbolTable extends HashMap<String, Variable> {
    public SymbolTable makeCopy() {
        SymbolTable newTable = new SymbolTable();
        newTable.putAll(this);
        return newTable;
    }
}