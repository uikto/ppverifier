package de.theminefighter.pp_verifier.synthesis;

import de.theminefighter.pp_verifier.XsdData.Type;

/**
 * Represents a node in the semantic tree
 * Note that validations of type compatibility are executed in at least one of get getXPath and getExpType.
 * After calling both of these methods both methods must result in both of them being called on all the children of a node.
 * Any implementation must ensure that
 */
public abstract class SemanticTreeNode {
    public abstract String getXPath(SymbolTable st);
    public abstract Type getExpType(SymbolTable st);

   // public void checkLegality(SymbolTable st) {};
}
