package de.theminefighter.pp_verifier.synthesis;

import de.theminefighter.pp_verifier.XsdData.Type;
import org.jetbrains.annotations.Contract;

/**
 * Represents a variable in PPVL, might become a record at some point
 */
public class Variable {
    @Contract(pure = true)
    public Variable(String schematronName, Type type) {
        this.schematronName = schematronName;
        this.type = type;
    }

    public final String schematronName;
    public final Type type;

    //public SemanticTreeNode Origin;
}
