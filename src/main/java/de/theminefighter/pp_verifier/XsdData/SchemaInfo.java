package de.theminefighter.pp_verifier.XsdData;

import com.sun.xml.xsom.*;
import com.sun.xml.xsom.parser.XSOMParser;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.UnmodifiableView;
import org.jetbrains.annotations.VisibleForTesting;
import org.xml.sax.SAXException;

import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.math.BigInteger;
import java.util.*;

/**
 * Provides infos about the PlanPro XMl Schema Definition, and som methods to interact with it
 */
public class SchemaInfo {
    public static final String xmlSchemaLocation = "http://www.w3.org/2001/XMLSchema";
    static final XSSchemaSet xsss = LoadSchema();
    public static final XSType decimal = xsss.getType(xmlSchemaLocation, "decimal");
    public static final XSType any = xsss.getType(xmlSchemaLocation, "anyType");
    public static final XSType string = xsss.getType(xmlSchemaLocation, "string");
    public static final XSType anyAtom = xsss.getType(xmlSchemaLocation, "anySimpleType");
    public static final Map<String, Set<XSType>> refTypes = Collections.unmodifiableMap(RefTypes.getPossibleRefTypes());
    public static final Map<String, XSType> implicitResolve =
            Collections.unmodifiableMap(RefTypes.implicitRefResolver(refTypes));
    public static final XSType bool = xsss.getType(xmlSchemaLocation, "boolean");
    private static final Map<String, Map<String, Type>> attributeMap = BuildAttributeMap();

    private static final Map<String, XSType> stringToProp = BuildStringTable(attributeMap);
    static final String schemasFolder = "target/classes/ppSchema";

    private SchemaInfo() {
    }

    private static Map<String, Map<String, Type>> BuildAttributeMap() {
        Map<String, Map<String, Type>> attrs = new HashMap<>();
        XSSchemaSet xsSchemaSet = LoadSchema();
        for (Iterator<XSComplexType> it = xsSchemaSet.iterateComplexTypes(); it.hasNext(); ) {
            XSComplexType type = it.next();
            Map<String, Type> typeAttributeMap = new HashMap<>();
            String k = TypeToStr(type);
            attrs.put(k, typeAttributeMap);
            XSTerm term = type.getContentType().asParticle().getTerm();
            ProcessTerm(term, typeAttributeMap, false);
            //String typeName = TypeNameCleaner(type.getName());

        }
        return attrs;
    }

    private static Map<String, XSType> BuildStringTable(Map<String, Map<String, Type>> attrMap) {
        Map<String, XSType> result = new HashMap<>();
        for (Map<String, Type> entry : attrMap.values()) {
            for (Map.Entry<String, Type> entry1 : entry.entrySet()) {
                String name = entry1.getKey();
                if (result.containsKey(name)) {
                    result.put(name, null);
                } else {
                    result.put(name, entry1.getValue().xmlType());
                }
            }
        }
        return result;
    }

    private static void ProcessTerm(XSTerm term, Map<String, Type> typeAttributeMap, boolean isSet) {
        if (term.isModelGroup()) {
            for (XSParticle xsp : term.asModelGroup().getChildren()) {
                BigInteger maxOccurs = xsp.getMaxOccurs();
                boolean maxOneOccurs = BigInteger.ONE.equals(maxOccurs);
                ProcessTerm(xsp.getTerm(), typeAttributeMap, !maxOneOccurs);
            }
        } else if (term.isElementDecl()) {
            Type attrType = new Type(term.asElementDecl().getType(), isSet);
            String name = term.asElementDecl().getName();
            typeAttributeMap.put(name, attrType);
        } else if (term.isWildcard()) {
            //any type
            //no action needed
        } else throw new RuntimeException("Unexpected element in xsd");

    }

    @Contract(pure = true)
    private static XSSchemaSet LoadSchema() {
        XSOMParser parser = new XSOMParser(SAXParserFactory.newInstance());
        for (File f : getPpSchemas()) {
            try {
                parser.parse(f);
            } catch (Exception e) {
                throw new RuntimeException("Error whilst loading PlanPro XSD files: " + e);
            }
        }
        try {
            return parser.getResult();
        } catch (SAXException e) {
            throw new RuntimeException("Error whilst loading PlanPro XSD files: " + e);
        }
    }

    @VisibleForTesting
    public static File[] getPpSchemas() {
        return new File(schemasFolder).listFiles((d, name) -> name.endsWith(".xsd"));
    }

    @Contract(pure = true)
    public static XSType getTypeFromName(String name) {
        XSType xsType = stringToProp.get(name);
        if (xsType == null)
            throw new SynthesisException("Type not found: " + name);
        return xsType;
    }

    @Contract(pure = true)
    public static boolean isTypeEqual(XSType t1, XSType t2) {
        if (t1 == null)
            System.out.println("issue");
        return t1.getName().equals(t2.getName()) && t1.getTargetNamespace().equals(t2.getTargetNamespace());
    }

    @Contract(pure = true, value = "_ -> new")
    public static String TypeToStr(XSType t) {
        return t.getTargetNamespace() + ":" + t.getName();
    }

    @Contract(pure = true)
    public static boolean isDerivedFromAlt(XSType derivedType, XSType basetype) {
        if (isTypeEqual(basetype, any))
            return true;
        while (!isTypeEqual(derivedType, any)) {
            if (isTypeEqual(basetype, derivedType))
                return true;
            derivedType = derivedType.getBaseType();
        }
        return false;
    }

    @Contract(pure = true)
    public static @NotNull @UnmodifiableView Map<String, Type> getAttributesOf(XSType type) {
        return Collections.unmodifiableMap(attributeMap.get(TypeToStr(type)));
    }

    @Contract(pure = true)
    public static XSType getType(String ns, String name) {
        return xsss.getType(ns, name);
    }
}
