package de.theminefighter.pp_verifier.XsdData;

import com.sun.xml.xsom.XSType;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static de.theminefighter.pp_verifier.XsdData.SchemaInfo.TypeToStr;

/**
 * Provides Infos about the possible types a TCZeiger can point to, only used by @see SchemaInfo, and intentionally not public
 */
class RefTypes {
    @Contract(pure = true)
    private static @NotNull Map<String, String> buildFixRefMap() {
        //I know doing this using XPath is not a nice solution,
        // but I tried for multiple hours to get this working with xsom and xmlschema-core and did get it to work with either.
        // At least for xsom i am quite sure that there is some bug.
        Document xmlDocument;
        NodeList typeNames;
        NodeList fixedVals;
        try {
            FileInputStream fileIS = new FileInputStream(SchemaInfo.schemasFolder + "/Verweise.xsd");
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            xmlDocument = builder.parse(fileIS);
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression fixedXP = xPath.compile("/*/*/*/*/*/@fixed");
            XPathExpression typeNameXP = xPath.compile("/*/*/@name");
            fixedVals = (NodeList) fixedXP.evaluate(xmlDocument, XPathConstants.NODESET);
            typeNames = (NodeList) typeNameXP.evaluate(xmlDocument, XPathConstants.NODESET);
        } catch (XPathExpressionException | SAXException | IOException | ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
        assert fixedVals.getLength() == typeNames.getLength();
        Map<String, String> ret = new HashMap<>();
        for (int i = 0; i < fixedVals.getLength(); i++) {
            ret.put(typeNames.item(i).getNodeValue(), fixedVals.item(i).getNodeValue());
        }
        return ret;
    }

    @Contract(pure = true)
    private static Map<String, XSType> getTypesFromNamesCur(){
        Map<String, XSType> res= new HashMap<>();
        for (XSType t: CommonTypes.cUrObjekt.listSubstitutables() ) {
            res.put(t.getName().substring(1),t);//Trim C Prefix
        }
        return res;
    }

    @Contract(pure = true)
    private static Map<String, XSType> getTypesFromNames(){
        Map<String, XSType> res= new HashMap<>();
        for (Iterator<XSType> it = SchemaInfo.xsss.iterateTypes(); it.hasNext(); ) {
            XSType t = it.next();
            res.put(t.getName(),t);
        }
        return res;
    }

    @Contract(pure = true)
    static Map<String, XSType> implicitRefResolver(Map<String, Set<XSType>> possibleRefTypes){
        Map<String, XSType> res= new HashMap<>();
        for (Map.Entry<String, Set<XSType>> e: possibleRefTypes.entrySet()){
            if (e.getValue().size()==1) {
                res.put(e.getKey(), e.getValue().stream().findFirst().get());
            }
        }
        return res;
    }

    @Contract(pure = true)
    static Map<String, Set<XSType>> getPossibleRefTypes(){
        Map<String, Set<XSType>> res= new HashMap<>();
        Map<String, XSType> typesFromNames = getTypesFromNames();
        Map<String, XSType> typesFromNamesCur = getTypesFromNamesCur();
        Map<String, String> strdata=buildFixRefMap();
        for (Map.Entry<String, String> e: strdata.entrySet()){
            List<String> possibleTargets = new ArrayList<>(Arrays.asList(e.getValue().split("\\|")));
            possibleTargets.removeIf("Proxy_Objekt"::equals);
            Set<XSType> setOfTargets = possibleTargets.stream().map(typesFromNamesCur::get).collect(Collectors.toSet());
            res.put(TypeToStr(typesFromNames.get(e.getKey())), setOfTargets);
        }
        return res;
    }
}
