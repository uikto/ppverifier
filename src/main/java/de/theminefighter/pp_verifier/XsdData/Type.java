package de.theminefighter.pp_verifier.XsdData;

import com.sun.xml.xsom.XSType;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * Represents a type from PPVL
 * @param xmlType the underlying xml/xsdtype
 * @param isSet whether the type is a set of the xmltype or a singular item
 */
public record Type(@NotNull XSType xmlType, boolean isSet) {
    public static final Type singleBool = new Type(SchemaInfo.bool, false);
    public static final Type singleNum = new Type(SchemaInfo.decimal, false);
    public static final Type singleStr = new Type(SchemaInfo.string, false);

    @Contract(pure = true, value = "_ -> new")
    public static String @NotNull [] splitXsdType(String url) {
        int i = url.lastIndexOf('/');
        return new String[]{url.substring(0, i), url.substring(i)};
    }

    public static @NotNull String getTypeURI(XSType type) {
        return type.getTargetNamespace() + '/' + type.getName();
    }

    @Contract(pure = true)
    public boolean isSingleNum() {
        return !isSet && SchemaInfo.isDerivedFromAlt(xmlType, SchemaInfo.decimal) ;
    }
    @Contract(pure = true)
    public boolean isSingleBool() {
        return !isSet && SchemaInfo.isDerivedFromAlt(xmlType, SchemaInfo.bool) ;
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Type type = (Type) o;
        return isSet == type.isSet &&
                type.xmlType.getName().equals(xmlType.getName()) &&
                type.xmlType.getTargetNamespace().equals(xmlType.getTargetNamespace());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isSet, xmlType.getName(), xmlType.getTargetNamespace());
    }

    @Contract(pure = true)
    @Override
    public @NotNull String toString() {
        return (isSet?"Set:":"Single:")+ SchemaInfo.TypeToStr(xmlType);
    }

    @Contract("_ -> new")
    public @NotNull Type getTypeFromAttr(String pathPart) {
        Type attrType = SchemaInfo.getAttributesOf(xmlType()).get(pathPart);
        if (attrType==null)
            throw new SynthesisException("attribute " + pathPart + " not found on " + SchemaInfo.TypeToStr(xmlType));
        return new Type(attrType.xmlType(), attrType.isSet() || isSet());
    }
    @Contract(value = "_ -> new", pure = true)
    public @NotNull Type orSet(boolean higherLevelSet) {
        return new Type(xmlType(), isSet()||higherLevelSet);
    }

    @Contract(value = "_ -> new", pure = true)
    public @NotNull Type withXmlType(XSType t) {
        return new Type(t, isSet());
    }
}
