package de.theminefighter.pp_verifier.XsdData;

import com.sun.xml.xsom.XSType;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Provides often used XSTypes from the PlanPro Schema and tools to obtain other types,
 * only place where PlanPro Version Data has to be changed
 */
public class CommonTypes {
    private static final String ppVersion = "1.10.0.1";
    private static final String signalbegriffeStrukturVersion="1.6.0.1";
    public static final XSType cUrObjekt = getPPType("Basisobjekte", "CUr_Objekt");
    public static final XSType guid = getPPType("BasisTypen", "TGUID");
    public static final XSType tcZeiger = getPPType("BasisTypen", "TCZeiger");
    public static final XSType cBasisAttribut = getPPType("BasisTypen", "CBasisAttribut");
    @Contract(pure = true)
    private static String getPpNs(String name) {
        return "http://www.plan-pro.org/modell/%s/%s".formatted(name,"Signalbegriffe_Struktur".equals(name)?signalbegriffeStrukturVersion:  ppVersion);
    }
    @Contract(pure = true)
    public static @NotNull XSType getPPType(String ns, String name) {
        XSType type = SchemaInfo.getType(getPpNs(ns), name);
        if (type==null)
            throw new SynthesisException("Type not found");
        return type;}
}
