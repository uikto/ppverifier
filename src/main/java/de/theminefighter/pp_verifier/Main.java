package de.theminefighter.pp_verifier;

import de.theminefighter.pp_verifier.schematron.Validation;
import de.theminefighter.pp_verifier.synthesis.PpvlConverter;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class Main {

    private static final Scanner scanner = new Scanner(System.in);
    public static final String testhausenPath = "target/classes/ESTW-Testhausen.planpro.xml";

    public static void main(String[] args) throws Exception {
        System.out.println("Gib PPVL Regeln und optional Variablen ein und beende die Eingabe mit zwei Zeilenumbrüchen (2x Enter):");
        String parseContent = readInput();
        String schematron = PpvlConverter.convertToSchematron(parseContent);
        final String schematronFileName = "out.sch";
        Files.writeString(Path.of(schematronFileName), schematron);
        System.out.printf("Ergebnis erfolgreich konvertiert und nach %s geschrieben:%n", schematronFileName);
        System.out.println(schematron);
        System.out.println("Um diesen Schematronregel(n) auf eine PlanPro Datei anzuwenden gib nun den Dateinamen ein," +
                " oder drücke Enter um ihn auf die PlanPro Datei vom ESTW-Testhausen anzuwenden.");
        if (!scanner.hasNextLine()) return;
        String s = scanner.nextLine();
        if ("".equals(s)) s= testhausenPath;
        boolean isValid= Validation.validateXMLViaXSLTSchematron(new File("out.sch"), new File(s));
        String output = "Die Planung erfüllt die Regel";
        if (!isValid) output+=" NICHT";
        System.out.println(output);
    }

    public static @NotNull String readInput() {
        StringBuilder stringBuilder = new StringBuilder();
        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            if (nextLine.isEmpty()) return stringBuilder.toString();
            stringBuilder.append(nextLine).append("\n");
        }
        return stringBuilder.toString();
    }
}