package de.theminefighter.pp_verifier.schematron;

import com.helger.schematron.ISchematronResource;
import com.helger.schematron.svrl.jaxb.SchematronOutputType;
import com.helger.schematron.xslt.SchematronResourceSCH;

import javax.annotation.Nonnull;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import name.dmaus.schxslt.*;

/**
 * Tools for testing schematron rules on xml files using different libraries
 */
public class Validation {
    public static boolean validateXMLViaXSLTSchematron (@Nonnull final File aSchematronFile, @Nonnull final File aXMLFile) throws Exception
    { //return validateHelger(aSchematronFile, aXMLFile);
return validateSchxslt(aSchematronFile, aXMLFile);
    }

    public static boolean validateHelger(@Nonnull final File aSchematronFile, @Nonnull final File aXMLFile) throws Exception
    {
        final ISchematronResource aResSCH = SchematronResourceSCH.fromFile (aSchematronFile);
        if (!aResSCH.isValidSchematron ())
            throw new IllegalArgumentException ("Invalid Schematron!");
        return aResSCH.getSchematronValidity (new StreamSource(aXMLFile)).isValid ();
    }

    public static SchematronOutputType validateXMLViaXSLTSchematronFull (@Nonnull final File aSchematronFile, @Nonnull final File aXMLFile) throws Exception
    {
        final ISchematronResource aResSCH = SchematronResourceSCH.fromFile (aSchematronFile);
        if (!aResSCH.isValidSchematron ())
            throw new IllegalArgumentException ("Invalid Schematron!");
        return aResSCH.applySchematronValidationToSVRL (new StreamSource (aXMLFile));
    }

    public static boolean validateSchxslt(@Nonnull final File aSchematronFile, @Nonnull final File aXMLFile) {
        try {
            Schematron schematron = new Schematron(new StreamSource(aSchematronFile));
            Result res = schematron.validate(new StreamSource(aXMLFile));
            return res.isValid();
        } catch (SchematronException e) {
            throw new RuntimeException(e);
        }
    }




}
