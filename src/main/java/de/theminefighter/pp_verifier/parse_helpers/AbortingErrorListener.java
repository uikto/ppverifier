package de.theminefighter.pp_verifier.parse_helpers;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.ParseCancellationException;

/**
 * ANTLR Error listener throwing the error
 */
public class AbortingErrorListener extends BaseErrorListener {

    public static final AbortingErrorListener Singleton = new AbortingErrorListener();

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            throws ParseCancellationException {
        throw new ParseCancellationException("Parse error on line " + line + ":" + charPositionInLine + ": " + msg);
    }
}
