package de.theminefighter.pp_verifier;

import de.theminefighter.pp_verifier.XsdData.SchemaInfo;
import de.theminefighter.pp_verifier.XsdData.Type;
import de.theminefighter.pp_verifier.parser.PpVerifierDslBaseVisitor;
import de.theminefighter.pp_verifier.parser.PpVerifierDslParser;
import de.theminefighter.pp_verifier.synthesis.SemanticTreeNode;
import de.theminefighter.pp_verifier.synthesis.SymbolTable;
import de.theminefighter.pp_verifier.synthesis.SynthesisException;
import de.theminefighter.pp_verifier.synthesis.Variable;
import org.jetbrains.annotations.Contract;

/**
 * A visitor to visit PPVl statements and convert them to parts of a Schematron file using the @see SemanticTreeVisitor
 */
public class SchematronVisitor extends PpVerifierDslBaseVisitor<String> {
    final SemanticTreeVisitor astBuilder = new SemanticTreeVisitor();
    final SymbolTable currentSt = new SymbolTable();

    @Override
    public String visitFullDoc(PpVerifierDslParser.FullDocContext ctx) {
        return String.join("\n\t\t\t", ctx.statement().stream().map(this::visit).toList());

    }

    @Override
    public String visitSingleRuleDoc(PpVerifierDslParser.SingleRuleDocContext ctx) {
        return ruleToSchematron(ctx.expr());
    }

    @Override
    @Contract(value = "_ -> new", mutates="this")
    public String visitVarDef(PpVerifierDslParser.VarDefContext ctx) {
        SemanticTreeNode varExpr = astBuilder.visit(ctx.expr());
        String xpath = varExpr.getXPath(currentSt);
        String varName = ctx.Ident().getText();
        currentSt.put(varName, new Variable('$'+varName, varExpr.getExpType(currentSt)));
        return "<let name=\"%s\" value=\"%s\"/>".formatted(varName, xpath);
    }

    @Override
    public String visitRuleDef(PpVerifierDslParser.RuleDefContext ctx) {
        return ruleToSchematron(ctx.expr());
    }

    private String ruleToSchematron(PpVerifierDslParser.ExprContext ruleExpr) {
        SemanticTreeNode varExpr = astBuilder.visit(ruleExpr);
        Type expType = varExpr.getExpType(currentSt);
        if (expType.isSingleNum() || !SchemaInfo.isTypeEqual(expType.xmlType(), SchemaInfo.bool))
            throw new SynthesisException("Rules must return a single boolean");
        return "<assert test=\"%s\"/>".formatted(varExpr.getXPath(currentSt));
    }
}
